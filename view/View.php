<?php

abstract class View {


    public function set($name, $value)
    {
        $this->$name = $value;
        
        return $this;
    }
    
    public function get ($name)
    {
        return $name;
    }
    
    public function renderTemplate($name, $path='templates/')
    {
        $templatePath = $path . $name . '.html.php';
        
        try {
            if (!file_exists($templatePath)) {
                throw new Exception('Can not render template ' . $name);
            } else {
                require $templatePath;
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
    }
}
