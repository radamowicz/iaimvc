<?php
require_once 'Controller.php';

/**
 * Created by PhpStorm.
 * User: msnowak
 * Date: 31.05.17
 * Time: 16:13
 */
class ClientsController extends Controller
{
    public function getClients(){

        $model = $this->loadModel('Clients');
        $data = $model->getAllData();
        echo json_encode($data);
    }
}