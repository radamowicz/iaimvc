<?php
require_once 'Controller.php';
/**
 * Created by PhpStorm.
 * User: msnowak
 * Date: 31.05.17
 * Time: 17:05
 */
class NewsLetController extends Controller
{
    public function index(){
        $users = json_decode(file_get_contents('http://localhost/web/iaimvc/?controller=Clients&action=getClients'));

        $products = json_decode(file_get_contents('http://localhost/web/iaimvc/?controller=Products&action=getProducts'));

        $msgArr = [];

        foreach ($users as $user){
            $singleMsg = [];
            $singleMsg["mailTo"] = $user->email;
            $userProducts = [];
            //echo $user->email.' - ';
            foreach ($products as $product){
                $singleProd = [];
                $singleProd["name"] = $product->name;
                $singleProd["price"] = $product->price;
                array_push($userProducts,$singleProd);
            }
            $singleMsg["prod"] = $products;
            array_push($msgArr,$singleMsg);
        }

        echo json_encode($msgArr);
    }
}