<?php
require_once 'Model.php';

class ProductsModel extends Model{

    public function getAllData()
    {
        $result = $this->pdo->prepare("SELECT id, name, price FROM products");
        $result->execute();

        $arr = $result->fetchAll(PDO::FETCH_ASSOC);

        return $arr;
    }
}

