<?php
require_once 'Model.php';


class ClientsModel extends Model
{
    public function getAllData()
    {
        $result = $this->pdo->prepare("SELECT `name`, `surname`,`email`,`date_of_birth` FROM clients");
        $result->execute();

        $arr = $result->fetchAll(PDO::FETCH_ASSOC);

        return $arr;
    }
}